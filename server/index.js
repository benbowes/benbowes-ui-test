const express = require('express');
const app = express();
const PORT = 3002;

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3001' );
  res.header('Access-Control-Allow-Headers', 'GET');
  next();
});

app.get('/api/search', function(req, res) {
  res.sendFile(__dirname + '/response.json');
});

app.get('/404/response', function(req, res) {
  res.sendFile(__dirname + '/noFileExistsHere.json');
});

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.set('port', PORT);
app.listen(PORT, function() {
  console.log('Mock api server is running at http://localhost:' + PORT + '\r\n\r\n');
});
