import React, { PropTypes } from 'react';
import * as styles from './PropertyListingHeader.scss';

/**
* @description Stateless function component.
* @returns {JSX} */

export const PropertyListingHeader = ( props ) => {
  // I'd probably have 2 different components.
  const agency = props.agency || {};
  const brandingColorsPrimary = (agency.brandingColors && agency.brandingColors.primary);
  const isAgencyHeader = (agency.logo && brandingColorsPrimary);

  return (
      <div>
        {isAgencyHeader &&
        <header
          className={styles['property-listing__agency-header']}
          style={{backgroundColor: brandingColorsPrimary}}
        >
          <img
            className={styles['property-listing__agency-header__img']}
            src={agency.logo}
            alt="Agency logo"
          />
        </header>
        }
      </div>
  );
};

PropertyListingHeader.propTypes = {
  agency: PropTypes.shape({
    brandingColors: PropTypes.shape({
      primary: PropTypes.string
    }),
    logo: PropTypes.string
  })
};

export default PropertyListingHeader;
