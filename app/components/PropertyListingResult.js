import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import * as actionTypes from 'constants/actionTypes';
import * as styles from './PropertyListingSaved.scss';
import PropertyListingHeader from './PropertyListingHeader';
import { PreloadFadeInImage } from './common/PreloadFadeInImage';
import PropertyListingButton from './PropertyListingButton';
import PropertyListingFooter from './PropertyListingFooter';

/**
* @description Stateless function component.
* @returns {JSX} */

export const PropertyListingResult = ( props ) => {
  const { property, index, dispatch } = props;

  return (
    <li tabIndex="0" aria-label={`Search result number ${index + 1}`}>
      <article>
        <PropertyListingHeader agency={property.agency} />
        <div className={styles['property-listing']}>
          <PreloadFadeInImage imgSauce={property.mainImage} alt={`Image of property result ${index + 1}`} />
          <PropertyListingButton
            ariaLabel="Add this property to your saved properties"
            labelText="Add property"
            toasterMessage="A property has been added to your saved properties"
            property={property}
            dispatch={dispatch}
            actionType={actionTypes.ADD_SAVED_PROPERTY}
          />
        </div>
        <PropertyListingFooter price={property.price} />
      </article>
    </li>
  );
};

PropertyListingResult.propTypes = {
  index: PropTypes.number,
  property: PropTypes.shape({
    mainImage: PropTypes.string,
    id: PropTypes.string,
    agency: PropTypes.shape({
      'brandingColors': PropTypes.shape({
        'primary': PropTypes.string
      }),
      logo: PropTypes.string
    })
  }),
  price: PropTypes.string,
  dispatch: PropTypes.func
};

// Grab store.dispatch() without any state
export default connect()(PropertyListingResult);
