import React, { PropTypes, Component } from 'react';
import * as styles from './PreloadFadeInImage.scss';

/**
* @description class component - fades img in, once loaded
* @returns {JSX} */

export class PreloadFadeInImage extends Component {

  constructor() {
    super();
    this.state = { isLoading: true };
  }

  handleImageLoaded() {
    this.setState({ isLoading: false });
  }

  render() {
    const { imgSauce, alt } = this.props;
    const loadingStyle = { opacity: '0' };

    return (
      <div className={styles['image']}>
        <img
          alt={alt}
          onLoad={() => this.handleImageLoaded()}
          className={styles['image__img']}
          src={imgSauce}
          style={this.state.isLoading ? loadingStyle: {}}
        />
      </div>
    );
  }
}

PreloadFadeInImage.propTypes = {
  imgSauce: PropTypes.string,
  alt: PropTypes.string
};
