import jsdom from 'mocha-jsdom';

import 'ignore-styles';
import expect, { createSpy } from 'expect';
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import PropertyListingButton from './PropertyListingButton';

describe('<PropertyListingButton />', () => {

  jsdom();

  it('Should render as expected', () => {
    const renderer = ReactTestUtils.createRenderer();
    renderer.render(
      <PropertyListingButton
        ariaLabel="Some aria label"
        actionType="SOME_ACTION_TYPE"
        labelText="Some label text"
      />
    );
    const ReactComponent = renderer.getRenderOutput();

    expect(ReactComponent.props.type).toEqual('button');
    expect(ReactComponent.props['aria-label']).toEqual('Some aria label');
    expect(ReactComponent.props.className).toEqual('property-listing__button');
    expect(ReactComponent.props.children).toEqual('Some label text');
  });

  it('It should call dispatch() with the expected params when clicked', () => {
    const spy = createSpy();
    const renderedOutput = ReactTestUtils.renderIntoDocument(
      <PropertyListingButton
        actionType="SOME_ACTION_TYPE"
        dispatch={spy}
        toasterMessage="Some toaster message"
        property={{id: '2', image: 'http://someimageurl'}}
      />
    );

    // Get component DOM
    const ReactComponent = ReactTestUtils.scryRenderedDOMComponentsWithTag(
      renderedOutput, 'button'
    );
    const propertyListingButtonDOM = ReactComponent[0];

    // Click on the button
    ReactTestUtils.Simulate.click(propertyListingButtonDOM);

    // Assert that dispatch was called with expected params
    expect(spy.calls.length).toEqual(1);
    expect(spy).toHaveBeenCalledWith({
      type: 'SOME_ACTION_TYPE',
      payload: {
        property: {id: '2', image: 'http://someimageurl'},
        toasterMessage: 'Some toaster message'
      }
    });
  });

});
