import React, { PropTypes, Component } from 'react';
import './PropertyListingButton.scss';

/**
* @description class component
* @returns {JSX} */

export default class PropertyListingButton extends Component {

  handleClick(){
    const { dispatch, actionType, property, toasterMessage } = this.props;
    dispatch({
      type: actionType,
      payload: {
        property: property,
        toasterMessage: toasterMessage
      }
    });
  }

  render() {
    const { ariaLabel, labelText } = this.props;
    return (
      <button
        type="button"
        aria-label={ariaLabel}
        onClick={() => this.handleClick()}
        className="property-listing__button"
      >
        {labelText}
      </button>
    );
  }
}

PropertyListingButton.propTypes = {
  ariaLabel: PropTypes.string,
  dispatch: PropTypes.func,
  actionType: PropTypes.string,
  labelText: PropTypes.string,
  toasterMessage: PropTypes.string,
  property: PropTypes.object
};
