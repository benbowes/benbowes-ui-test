import 'ignore-styles';
import expect from 'expect';
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import { PropertyListingHeader } from './PropertyListingHeader';

describe('<PropertyListingHeader />', () => {

  it('Should render children when expected props are present', () => {
    const renderer = ReactTestUtils.createRenderer();
    renderer.render(
      <PropertyListingHeader
        agency={{
          brandingColors: { primary: '#333333' },
          logo: 'http://somelogourl'
        }}
      />
    );
    const ReactComponent = renderer.getRenderOutput();

    // <div />
    expect(ReactComponent.type).toEqual('div');

    // <header />
    expect(ReactComponent.props.children.type).toEqual('header');
    expect(ReactComponent.props.children.props.style).toEqual({ backgroundColor: '#333333' });

    // <img />
    expect(ReactComponent.props.children.props.children.type).toEqual('img');
    expect(ReactComponent.props.children.props.children.props.alt).toEqual('Agency logo');
    expect(ReactComponent.props.children.props.children.props.src).toEqual('http://somelogourl');
  });


  it('Should NOT render children when agency.logo is missing from props', () => {
    const renderer = ReactTestUtils.createRenderer();
    renderer.render(
      <PropertyListingHeader
        agency={{
          brandingColors: { primary: '#333333' }
        }}
      />
    );
    const ReactComponent = renderer.getRenderOutput();

    // <div />
    expect(ReactComponent.type).toEqual('div');
    expect(ReactComponent.props.children).toEqual(undefined);
  });

  it('Should NOT render children when agency.brandingColors is missing from props', () => {
    const renderer = ReactTestUtils.createRenderer();
    renderer.render(
      <PropertyListingHeader
        agency={{
          logo: 'http://somelogourl'
        }}
      />
    );
    const ReactComponent = renderer.getRenderOutput();

    // <div />
    expect(ReactComponent.type).toEqual('div');
    expect(ReactComponent.props.children).toEqual(undefined);
  });
});
