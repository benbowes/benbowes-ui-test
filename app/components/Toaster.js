import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import * as styles from './Toaster.scss';
/**
* @description Stateless function component.
* Written with screen readers in mind. When a user clicks add or remove propety buttons
* Focus is drawn to the toaster modal so that they can be read instructions as to
* what has changed in the interface. Clicking close returns them back to the
* beginning of the app - the `Results` heading.
* @returns {JSX} */

export class Toaster extends Component {

  /**
  * @description If the message is populated focus on this modals close button. */

  componentDidUpdate(){
    const { msg, hideFromScreenReader } = this.props;
    if( msg !== '' && !hideFromScreenReader ){
      ReactDOM.findDOMNode(this.refs.closeModalButton).focus();
    }
  }

  /**
  * @description Clicking the close button retuns user back to the beginning of the app. */

  closeModal() {
    document.querySelector('#search-results-heading').focus();
  }

  render() {
    const { msg, hideFromScreenReader } = this.props;
    return (
      <div
        style={{display: ( hideFromScreenReader ? 'none': 'inherit' )}}
        className={`${styles['toaster']} ${( msg ==='' ? 'screenReaderOnly' : '' )}`}
        role="alertdialog"
        aria-labelledby="alertHeading"
        aria-describedby="alertText"
        tabIndex="0"
      >
        <div id="firstElement" tabIndex="0"></div>
        <h1 id="alertHeading" className="screenReaderOnly">The screen has updated</h1>
        <span role="presentation" aria-hidden="true" className={styles['toaster-icon']}>&#x2139;</span>
        <p id="alertText">{msg}</p>
        <button
          title="Close modal"
          ref="closeModalButton"
          onClick={() => this.closeModal()}
          className="screenReaderOnly">
          <span role="presentation" aria-hidden="true">&times;</span>
        </button>
      </div>
    );
  }
}

Toaster.propTypes = {
  msg: PropTypes.string,
  hideFromScreenReader: PropTypes.bool
};

export default connect((state) => {
  return {
    msg: state.toaster.msg,
    hideFromScreenReader: state.toaster.hideFromScreenReader
  };
})(Toaster);
