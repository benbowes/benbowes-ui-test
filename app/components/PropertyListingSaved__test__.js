import 'ignore-styles';
import expect from 'expect';
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import { PropertyListingSaved } from './PropertyListingSaved';

const propertyObj = {
  mainImage: 'http://someimageurl',
  id: '3',
  agency: {
    brandingColors: { primary: '#333333' },
    logo: 'http://somelogourl'
  }
};

describe('<PropertyListingSaved />', () => {

  it('Should render as expected', () => {
    const renderer = ReactTestUtils.createRenderer();
    renderer.render(
      <PropertyListingSaved
        index={0}
        price="120,000"
        property={propertyObj}
      />
    );
    const ReactComponent = renderer.getRenderOutput();

    // <li />
    expect(ReactComponent.type).toEqual('li');
    expect(ReactComponent.props.tabIndex).toEqual('0');
    expect(ReactComponent.props['aria-label']).toEqual('Saved Property number 1');

    // <article />
    expect(ReactComponent.props.children.type).toEqual('article');

    // <PropertyListingHeader />
    expect( ReactComponent.props.children.props.children[0].type.name ).toEqual( 'PropertyListingHeader' );
    expect(ReactComponent.props.children.props.children[0].props.agency).toEqual( propertyObj.agency );

    // TODO: Check the rest of the rendered compoment has expected output or deal with individual components?

  });
});
