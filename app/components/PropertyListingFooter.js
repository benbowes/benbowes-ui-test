import React, { PropTypes } from 'react';
import * as styles from './PropertyListingFooter.scss';

/**
* @description Stateless function component.
* @returns {JSX} */

export const PropertyListingFooter = ( props ) =>
  <footer>
    <div className={styles['property-listing__footer']}>
      <p><strong>Price</strong>: {props.price}</p>
    </div>
  </footer>;

PropertyListingFooter.propTypes = {
  price: PropTypes.string
};

export default PropertyListingFooter;
