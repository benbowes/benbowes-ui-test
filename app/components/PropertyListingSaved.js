import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../constants/actionTypes';
import * as styles from './PropertyListingSaved.scss';
import PropertyListingHeader from './PropertyListingHeader';
import { PreloadFadeInImage } from './common/PreloadFadeInImage';
import PropertyListingButton from './PropertyListingButton';
import PropertyListingFooter from './PropertyListingFooter';

/**
* @description Stateless function component.
* @returns {JSX} */

export const PropertyListingSaved = ( props ) => {
  const { property, index, dispatch } = props;

  return (
    <li tabIndex="0" aria-label={`Saved Property number ${index + 1}`}>
      <article>
        <PropertyListingHeader agency={property.agency} />
        <div className={styles['property-listing']}>
          <PreloadFadeInImage imgSauce={property.mainImage} alt={`Image of saved property ${index + 1}`} />
          <PropertyListingButton
            ariaLabel="Remove this property from your saved properties"
            labelText="Remove property"
            toasterMessage="A property has been removed from your saved properties"
            property={property}
            dispatch={dispatch}
            actionType={actionTypes.REMOVE_SAVED_PROPERTY}
          />
        </div>
        <PropertyListingFooter price={property.price} />
      </article>
    </li>
  );
};

PropertyListingSaved.propTypes = {
  index: PropTypes.number,
  property: PropTypes.shape({
    mainImage: PropTypes.string,
    id: PropTypes.string,
    agency: PropTypes.shape({
      'brandingColors': PropTypes.shape({
        'primary': PropTypes.string
      }),
      logo: PropTypes.string
    })
  }),
  price: PropTypes.string,
  dispatch: PropTypes.func
};

// Grab store.dispatch() without any state
export default connect()(PropertyListingSaved);
