/*
* @param {Number} ms - Milliseconds to pause
* @param {Boolean} val - Promise resolve value
* @returns {Promise}
* @description Creates a pause that can be used between saga generators */

export const pausePromise = (ms, val=true) => {
  return new Promise(resolve => {
    setTimeout(() => resolve(val), ms);
  });
};
