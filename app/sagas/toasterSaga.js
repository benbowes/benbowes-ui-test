import { takeLatest } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import * as actionTypes from '../constants/actionTypes';
import { pausePromise } from './helpers/pausePromise';

/*
* @description recieves actionTypes.ADD_SAVED_PROPERTY's payload via action.
* toaster reducer then updates state.toaster.msg via SET_TOASTER_MESSAGE's payload.
* After 3 seconds it updates state.toaster.msg to '' which closes the toaster */

export function* updateToaster( action ) {
  yield put({type: actionTypes.SET_TOASTER_MESSAGE, payload: action.payload.toasterMessage });
  yield call( pausePromise, 3000 );
  yield put({type: actionTypes.SET_TOASTER_MESSAGE, payload: '' });
}

/*
* @description Listens for when an actionTypes.SHOW_TOASTER action is fired
* It calls updateToaster() everytime it does. Cancelelling the last saga.
* It passes in the redux action payload with it.

* Note: I am using `takeLatest` here (instead of `takeEvery`) as I want to cancel any
* previous calls to the updateToaster() saga. Otherwise the toaster may close sooner than expected.
* I want a new toaster message to be set on every action, and pausePromise to cancel, thus adding a
* percieved additional 3000ms before setting the message to '' which closes the toaster */

export function* showToasterOnAddProperty() {
  yield takeLatest( actionTypes.ADD_SAVED_PROPERTY, updateToaster );
}

/*
* @description Listens for when an actionTypes.SHOW_TOASTER action is fired
* It calls updateToaster() everytime it does. Cancelelling the last saga.
* It passes in the redux action payload with it.

* Note: I am using `takeLatest` here (instead of `takeEvery`) as I want to cancel any
* previous calls to the updateToaster() saga. Otherwise the toaster may close sooner than expected.
* I want a new toaster message to be set on every action, and pausePromise to cancel, thus adding a
* percieved additional 3000ms before setting the message to '' which closes the toaster */

export function* showToasterOnRemoveProperty() {
  yield takeLatest( actionTypes.REMOVE_SAVED_PROPERTY, updateToaster );
}
