import 'babel-polyfill'; // required by redux-saga for generator func's

import expect from 'expect';
import { take, call, put } from 'redux-saga/effects';
import * as actionTypes from '../constants/actionTypes';
import * as sagas from './apiSaga';
import { fetchWrapper } from './helpers/fetchWrapper';

describe(`Saga "apiSaga"`, () => {

  it(`It'll cycle through listenForDataRequests() forever...`, () => {
    const generator = sagas.listenForDataRequests();

    let result = generator.next();
    expect( result.value ).toEqual( take( actionTypes.REQUEST_SEARCH_DATA ) );

    result = generator.next();
    expect( result.value.FORK.fn ).toEqual( sagas.fetchData );

    result = generator.next();
    // Cycles back to first step in saga
    expect( result.value ).toEqual( take( actionTypes.REQUEST_SEARCH_DATA ) );
    // And is still active
    expect( result.done ).toEqual( false );
  });

  it(`It'll get search data...`, () => {
    const action = { type: actionTypes.REQUEST_SEARCH_DATA, url: 'http://someurl.com' };
    const generator = sagas.fetchData(action);

    let result = generator.next();
    expect( result.value ).toEqual( call(fetchWrapper, action.url) );
    expect( result.value.CALL.fn ).toEqual( fetchWrapper );
    expect( result.value.CALL.args ).toEqual( 'http://someurl.com' );

    result = generator.next({ 'some': 'data', 'ok': true });
    expect( result.value ).toEqual(
      put({type: actionTypes.RECEIVE_SEARCH_DATA, payload: { 'some': 'data', 'ok': true } })
    );

    result = generator.next();
    expect( result.done ).toEqual( true );
  });

  it(`It'll pass the error and failed action when an api call fails...`, () => {
    const action = { type: actionTypes.REQUEST_SEARCH_DATA, url: 'http://someurl.com' };
    const generator = sagas.fetchData( action );

    generator.next();
    let result = generator.throw({ error: 'File not found' }).value;

    expect( result.PUT.action.type )
      .toEqual( actionTypes.RECEIVE_SEARCH_DATA_FAILED );
    expect( result.PUT.action.payload.error )
      .toEqual({ error: 'File not found' });
    expect( result.PUT.action.payload.failedAction )
      .toEqual({ type: actionTypes.REQUEST_SEARCH_DATA, url: 'http://someurl.com' });

    result = generator.next();
    expect( result.done ).toEqual( true );
  });

});
