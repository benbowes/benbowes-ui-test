import { takeEvery } from 'redux-saga';
import { put, call } from 'redux-saga/effects';
import * as actionTypes from '../constants/actionTypes';
import { fetchWrapper } from './helpers/fetchWrapper';

/*
* @description Passes the redux action's `url` through to fetchWrapper().
* Passes server error and failed action through via RECEIVE_SEARCH_DATA_FAILED action */

export function* fetchData( action ) {
  try {
    const receivedData = yield call( fetchWrapper, action.url );

    if ( receivedData.ok ) {
      yield put({ type: actionTypes.RECEIVE_SEARCH_DATA, payload: receivedData });
    } else {
      throw receivedData;
    }

  } catch(err) {

    yield put({
      type: actionTypes.RECEIVE_SEARCH_DATA_FAILED,
      payload: { error: err, failedAction: action }
    });
    //console.log( 'An error happenned :/', err );
  }
}

/*
* @description Listens for when an actionTypes.REQUEST_SEARCH_DATA action is fired
* It calls fetchData() everytime it does.
* It passes in the redux action payload with it to the fetchData() function above.

* Note: I am using `takeEvery` here as I presume multiple requests may be made concurrently.
* i.e. In a real world example, I might need to call multiple endpoints at the same time */

export function* listenForDataRequests() {
  yield* takeEvery( actionTypes.REQUEST_SEARCH_DATA, fetchData );
}
