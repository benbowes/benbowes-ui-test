/*
* @requires Redux Saga needs to be polyfilled by npm module `babel-polyfill`
* see app/index.js */

import { fork } from 'redux-saga/effects';
import { showToasterOnAddProperty, showToasterOnRemoveProperty } from './toasterSaga';
import { listenForDataRequests } from './apiSaga';

/*
* @description This is our entry function.
* It's fired in store/configureStore after middleware is applied */

export function* rootSaga() {
  yield [
    fork(listenForDataRequests),
    fork(showToasterOnAddProperty),
    fork(showToasterOnRemoveProperty)
  ]; // fork the sagas we want to init at application `firstload`
}
