/*
* @description initial state that we inject into our redux store at bootup.
*/

export const initialState = {};
