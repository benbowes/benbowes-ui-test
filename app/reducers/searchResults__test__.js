import expect from 'expect';
import { searchResults } from './searchResults';
import * as actionTypes from '../constants/actionTypes';

describe('Reducer "searchResults"', () => {

  it(`It should return default state`, () => {
    const result = searchResults(undefined, {});
    expect(result).toEqual([]);
  });

  // RECEIVE_SEARCH_DATA

  it(`It should populate state.searchResults with "saved" items when ${actionTypes.RECEIVE_SEARCH_DATA}`, () => {
    const state = [{ 'original': 'state', 'ob': 'ject...' }];
    const result = searchResults(state, {
      type: actionTypes.RECEIVE_SEARCH_DATA,
      payload: {
        'saved': [{ 'some': 'thing', 'lovely': 'goes here...' }],
        'results': [{ 'another': 'thing' }]
      }
    });
    expect(result).toEqual([
      { 'original': 'state', 'ob': 'ject...' },
      { 'another': 'thing' }
    ]);
    expect(state).toEqual([{ 'original': 'state', 'ob': 'ject...' }]); // should not mutate state
  });

  // ADD_SAVED_PROPERTY

  it(`It should remove selected item from state.searchResults when ${actionTypes.ADD_SAVED_PROPERTY}`, () => {
    const state = [
      { id: '1', 'property': 'XYZ' },
      { id: '2', 'property': 'ABC' },
      { id: '3', 'property': 'DEF' }
    ];
    const result = searchResults(state, {
      type: actionTypes.ADD_SAVED_PROPERTY,
      payload: { property: { 'id': '2' } }
    });
    expect(result).toEqual([
      { id: '1', 'property': 'XYZ' },
      { id: '3', 'property': 'DEF' }
    ]);
    expect(state).toEqual([
      { id: '1', 'property': 'XYZ' },
      { id: '2', 'property': 'ABC' },
      { id: '3', 'property': 'DEF' }
    ]); // should not mutate state
  });

});
