import { combineReducers } from 'redux';

import toaster from './toaster';
import savedProperties from './savedProperties';
import searchResults from './searchResults';

const rootReducer = combineReducers({
  toaster,
  savedProperties,
  searchResults
});

export default rootReducer;
