import * as actionTypes from '../constants/actionTypes';

/*
* @description redux reducer
* @param {Object} state
* @param {Object} action
* @param {Object} action.payload
* @param {String} action.type
* @returns {Array} a new un-mutated Array for state.savedProperties */

export const savedProperties = (state = [], action = {}) => {

  switch(action.type) {

  // Merge API response's `saved` into state
  case actionTypes.RECEIVE_SEARCH_DATA:
    return [
      ...state,
      ...action.payload.saved
    ];

  // Add property to end of state
  case actionTypes.ADD_SAVED_PROPERTY:
    return ( action.payload && action.payload.property )
      ? [ ...state, action.payload.property ]
      : state;

  // Remove property at selected `id` if action.id is set
  case actionTypes.REMOVE_SAVED_PROPERTY:
    return ( action.payload && action.payload.property && action.payload.property.id )
      ? state.filter(item => item.id !== action.payload.property.id)
      : state;

  default:
    return state;

  }
};

export default savedProperties;
