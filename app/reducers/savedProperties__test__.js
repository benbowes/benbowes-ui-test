import expect from 'expect';
import { savedProperties } from './savedProperties';
import * as actionTypes from '../constants/actionTypes';

describe('Reducer "savedProperties"', () => {

  it(`It should return default state`, () => {
    const result = savedProperties(undefined, {});
    expect(result).toEqual([]);
  });

  // RECEIVE_SEARCH_DATA

  it(`It should populate state.savedProperties with "saved" items when ${actionTypes.RECEIVE_SEARCH_DATA}`, () => {
    const state = [{ 'original': 'state', 'ob': 'ject...' }];
    const result = savedProperties(state, {
      type: actionTypes.RECEIVE_SEARCH_DATA,
      payload: {
        'saved': [{ 'some': 'thing', 'lovely': 'goes here...' }],
        'results': [{ 'another': 'thing' }]
      }
    });
    expect(result).toEqual([
      { 'original': 'state', 'ob': 'ject...' },
      { 'some': 'thing', 'lovely': 'goes here...' }
    ]);
    expect(state).toEqual([{ 'original': 'state', 'ob': 'ject...' }]); // should not mutate state
  });

  // ADD_SAVED_PROPERTY

  it(`It should add selected item at end of state.savedProperties when ${actionTypes.ADD_SAVED_PROPERTY}`, () => {
    const state = [{ 'original': 'state', 'ob': 'ject...' }];
    const result = savedProperties(state, {
      type: actionTypes.ADD_SAVED_PROPERTY,
      payload: { property: { 'selected': 'property', 'info': 'goes here...' } }
    });
    expect(result).toEqual([
      { 'original': 'state', 'ob': 'ject...' },
      { 'selected': 'property', 'info': 'goes here...' }
    ]);
    expect(state).toEqual([{ 'original': 'state', 'ob': 'ject...' }]); // should not mutate state
  });

  // REMOVE_SAVED_PROPERTY

  it(`It should remove selected item from state.savedProperties when ${actionTypes.REMOVE_SAVED_PROPERTY}`, () => {
    const state = [
      { id: '1', 'property': 'XYZ' },
      { id: '2', 'property': 'ABC' },
      { id: '3', 'property': 'DEF' }
    ];
    const result = savedProperties(state, {
      type: actionTypes.REMOVE_SAVED_PROPERTY,
      payload: { property: { 'id': '2' } }
    });
    expect(result).toEqual([
      { id: '1', 'property': 'XYZ' },
      { id: '3', 'property': 'DEF' }
    ]);
    expect(state).toEqual([
      { id: '1', 'property': 'XYZ' },
      { id: '2', 'property': 'ABC' },
      { id: '3', 'property': 'DEF' }
    ]); // should not mutate state
  });

});
