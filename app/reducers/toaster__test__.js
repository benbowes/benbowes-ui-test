import expect from 'expect';
import { toaster } from './toaster';
import * as actionTypes from '../constants/actionTypes';

describe('Reducer "toaster"', () => {

  it(`It should return default state`, () => {
    const result = toaster(undefined, undefined);
    expect(result).toEqual({
      msg: '',
      hideFromScreenReader: true
    });
  });

  it(`It should populate state.toaster with a message and set hideFromScreenReader to false when ${actionTypes.SET_TOASTER_MESSAGE}`, () => {
    const state = { msg: 'some message', hideFromScreenReader: true };
    const result = toaster(state, {
      type: actionTypes.SET_TOASTER_MESSAGE,
      payload: 'A different message'
    });
    expect(result).toEqual({ msg: 'A different message', hideFromScreenReader: false });
    expect(state).toEqual({ msg: 'some message', hideFromScreenReader: true }); // should not mutate state
  });
});
