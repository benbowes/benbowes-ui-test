import * as actionTypes from '../constants/actionTypes';

/*
* @description redux reducer.
* `hideFromScreenReader` is true until a first `msg` is set. This is to
* ensure a screen reader does not read it out on first load.
* @param {Object} state
* @param {Object} action
* @param {String} action.payload - toaster message string
* @param {String} action.type
* @returns {Object} a new un-mutated object for state.toaster */

export const toaster = ( state = { msg: '', hideFromScreenReader: true }, action = {} ) => {

  switch(action.type) {

  case actionTypes.SET_TOASTER_MESSAGE:
    return {
      ...state,
      msg: action.payload || '',
      hideFromScreenReader: false
    };

  default:
    return state;

  }
};

export default toaster;
