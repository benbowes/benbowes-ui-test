import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import * as actionTypes from 'constants/actionTypes';
import Toaster from 'components/Toaster';
import PropertyListingSaved from 'components/PropertyListingSaved';
import PropertyListingResult from 'components/PropertyListingResult';

/**
* @description ES6 class component. The root component...
* @returns {JSX} */

export class BaseComponent extends Component {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: actionTypes.REQUEST_SEARCH_DATA,
      url: 'http://localhost:3002/api/search'
      //url: 'http://localhost:3002/404/response'
    });
  }

  render() {
    const { searchResults, savedProperties } = this.props;
    return (
      <div className="wrap">
        <main className="row">

          <Toaster />

          <section id="search-results" className="col-xs-4 col-lg-3">
            <header tabIndex="0" id="search-results-heading"><h3>Results</h3></header>
            {!searchResults.length && <p>No results message goes here</p>}
            {searchResults.length > 0 &&
              <ol>
                {searchResults.map((property, index) =>
                  <PropertyListingResult
                      key={property.id}
                      property={property}
                      index={index}
                  />)
                }
              </ol>
            }
          </section>

          <section id="saved-properties" className="col-xs-8 col-lg-9">
            <header tabIndex="0" id="saved-properties-heading"><h3>Saved Properties</h3></header>
            {!savedProperties.length && <p>No saved properties message goes here</p>}
            {savedProperties.length > 0 &&
              <ol>
              {savedProperties.map((property, index) =>
                <PropertyListingSaved
                    key={property.id}
                    property={property}
                    index={index}
                />)
              }
              </ol>
            }
          </section>

        </main>
      </div>
    );
  }
}

BaseComponent.propTypes = {
  searchResults: PropTypes.array,
  savedProperties: PropTypes.array,
  dispatch: PropTypes.func
};

export default connect((state) => {
  return {
    searchResults: state.searchResults,
    savedProperties: state.savedProperties,
    dispatch: state.dispatch
  };
})(BaseComponent);
