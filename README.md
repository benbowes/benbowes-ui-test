
# Search test June 16'

Special consideration has been made for screen reader users in this implementation of this app. I've have only tested this in OSX's VoiceOver though.

What I am hoping, is that it'll show that I am comfortable with HTML as well as JS and CSS, and that I understand the relationship of these 3. How to manage focus, how to hide; but not hide from screen-readers. How to order content logically. How to architect an app for easy maintenance/feature-removal and progressive enhancement via composition.

I have added tests, but have have not repeated similar tests types.

As a bit of a stretch goal, I implemented an accessible toaster modal.

I have documented functions also, please look into those :)

#### 1) Install the dependencies.

```
npm install
```

#### 2) Start the app
```
npm start
```

This will boot up a dev server and a mock api server.

###### Dev server location:
[http://localhost:3001/webpack-dev-server/](http://localhost:3001/webpack-dev-server/)
For the local dev environment or via: [http://localhost:3001](http://localhost:3001) to view the app without the webpack dev server.

###### Mock API server location:
[http://localhost:3002](http://localhost:3002)

#### Lint the code (with eslint)
```
npm run lint
```

#### Run the unit tests
```
npm test
```
or with [watch](https://mochajs.org/#usage) flag.
```
npm test -- -w
```

#### Distribute the code
```
npm run dist
```

#### Node and NPM versions
This app was built with these versions of node and npm
- Node: v4.3.0
- npm: 2.14.12

A presumption has been made that I only need to distribute the JS.

-----------

### See all the available commands with:

```
npm run
```
